package zzu.xjc.http.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public static void info(String log){
        String thread = Thread.currentThread().getName();
        String date = dateFormat.format(new Date());
        String total = date + " [" + thread +"] [INFO] - " + log;
        System.out.println(total);
    }
    public static void warn(String log){
        String thread = Thread.currentThread().getName();
        String date = dateFormat.format(new Date());
        String total = date + " [" + thread +"] [WARN] - " + log;
        System.out.println(total);
    }
}
