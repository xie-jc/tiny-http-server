package zzu.xjc.http;

import java.util.Objects;

public class HttpEndPoint {
    String url;
    Http.Method method;

    private HttpEndPoint(String url, Http.Method method) {
        this.url = url;
        this.method = method;
    }

    static HttpEndPoint combine(String url, Http.Method method){
        return new HttpEndPoint(Http.Url.process(url),method);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HttpEndPoint that = (HttpEndPoint) o;
        return url.equalsIgnoreCase(that.url) && method == that.method;
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, method);
    }
}
