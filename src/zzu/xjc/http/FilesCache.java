package zzu.xjc.http;

import java.nio.ByteBuffer;
import java.util.List;

public interface FilesCache {
    List<ByteBuffer> get(String url);
    void put(String url,List<ByteBuffer> list);
    boolean contains(String url);
    boolean contains(List<ByteBuffer> list);
}
