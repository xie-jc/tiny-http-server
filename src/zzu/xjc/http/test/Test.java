package zzu.xjc.http.test;

import zzu.xjc.http.*;

import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException {
        HttpServer server = new HttpServer(80,1);
        server.serve("/hello", Http.Method.GET, (req, resp) -> {
            resp.writeText("Hello","World");
        });
        server.serve("/home", Http.Method.GET ,((req , resp ) -> {
            String name = req.getParam("name");
            int id = Integer.parseInt(req.getParam("id"));
            String client = req.getHeader(HttpRequest.Header.UserAgent);
            resp.writeText("This is your home, " + name + " id:" + id + " from " + client);
        }));

        server.staticResourceDir("D:\\self\\temp\\static");
        server.start();
    }
}
