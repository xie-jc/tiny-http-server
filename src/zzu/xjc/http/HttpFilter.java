package zzu.xjc.http;

public interface HttpFilter {
    boolean filter(HttpRequest req,HttpResponse resp);
}
