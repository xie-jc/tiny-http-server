package zzu.xjc.http;

import java.util.HashMap;

public class HttpSession {
    private HashMap<String,Object> map;
    final String SessionID;
    final Long expireTime;

    HttpSession(String SessionID, Long expireTime) {
        this.SessionID = SessionID;
        this.expireTime = expireTime;
    }

    public Object getAttribute(String key){
        if (map == null){
            return null;
        }
        return map.get(key);
    }
    public void setAttribute(String key , Object value){
        if (map == null){
            map = new HashMap<>();
        }
        map.put(key,value);
    }
}
