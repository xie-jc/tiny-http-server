package zzu.xjc.http;

import zzu.xjc.http.util.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class Http {
    private Http(){}
    static class MethodStr {
        private MethodStr(){}

        static final String PUT = "PUT";
        static final String GET = "GET";
        static final String POST = "POST";
        static final String DELETE = "DELETE";
    }
    public enum Method{
        GET,POST,PUT,DELETE,ANY;

        @Override
        public String toString() {
            switch (this) {
                case GET:
                    return MethodStr.GET;
                case POST:
                    return MethodStr.POST;
                case PUT:
                    return MethodStr.PUT;
                case DELETE:
                    return MethodStr.DELETE;
                case ANY:
                    return "ANY";
                default:
                    throw new IllegalArgumentException();
            }
        }

        public static Method fromString(String method){
            switch (method){
                case MethodStr.GET  : return GET;
                case MethodStr.POST : return POST;
                case MethodStr.PUT  : return PUT;
                case MethodStr.DELETE : return DELETE;
                case "ANY" : return ANY;
                default: Logger.warn(method);return GET;
            }
        }
    }
    static class Url {
        private Url(){}
        static String process(String url){
            String s = url.trim();
            if(!s.startsWith("/")){
                s = s + "/";
            }
            if (s.length() > 1 && s.endsWith("/")){
                s = s.substring(0 , s.length() - 1);
            }
            return url;
        }
    }

    static String getChannelAddress(SocketChannel channel) throws IOException {
        InetSocketAddress address = (InetSocketAddress) channel.getRemoteAddress();
        return "[" + address.getHostString() + ":" + address.getPort() +"]";
    }

}
