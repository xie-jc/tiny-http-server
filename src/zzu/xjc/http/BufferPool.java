package zzu.xjc.http;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

public interface BufferPool {
    ByteBuffer allocate();
    LinkedList<ByteBuffer> allocate(int n);
    void giveback(ByteBuffer buffer);
    void giveback(List<ByteBuffer> bufferList);
    int size();
    String log();
}
